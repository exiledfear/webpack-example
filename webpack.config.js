var path = require('path');

module.exports = {
    mode: "development",
    entry: './src/index.ts',
    watch: true,
    watchOptions: {
        aggregateTimeout: 300,
        //poll: 1000,
        ignored: /node_modules/
    },
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    resolve: {
        // Add `.ts` and `.tsx` as a resolvable extension.
        extensions: [".ts", ".tsx", ".js"]
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            }
        ]
    }
};
