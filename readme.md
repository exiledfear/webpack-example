# What is going on?
First Run
```bash
npm install
```
This will install all the depenencies to get webpack going with typescript.

```
./dist/bundle.js
```
Is what you can include at the end in your source files. This is the compiled version that will be ES5 compatibile code.

## Included dependencies
- Moment.js
- jQuery for experiementation
- TypeScript


## In PHP Storm or similar
You should notice an NPM tray somewhere on the edges of the IDE. This will let you run any of the commands listed in the `scripts`
part of the `package.json`. 

I recommend running `watch` to compile typescript on the fly without having to rerun the command everytime.